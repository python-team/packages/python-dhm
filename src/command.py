# command.py
#
# Copyright 2003 Wichert Akkerman <wichert@deephackmode.org>
#
# This file is free software; you can redistribute it and/or modify it
# under the terms of version 2 of the GNU General Public License as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# Calculate shared library dependencies

"""Command execution tools

The standard python library only implements lowlevel tools to run external
commands. This module builds on those primitives and adds more highlevel
tools to run and interface with external commands.
"""

__docformat__	= "epytext en"

import errno, fcntl, os, sys
import strtools


def _SetFdFlag(fd, flag):
	"""Set a filedescriptor flag.

	@param   fd: filedescriptor
	@type    fd: integer
	@param flag: flag to set
	@type  flag: integer
	"""
	flags=fcntl.fcntl(fd, fcntl.F_GETFL)
	fcntl.fcntl(fd, fcntl.F_SETFL, flags|flag)


class Command:
	"""Command executor

	This class represents an external command that can be executed. This
	is as a simple base class: it does nothing more than execute a
	command.  If you want to feed the command input or examine its
	output please use a derived class such as OutputCommand instead.

	There are a few methods that can be overriden in derived classes to
	customize behaviour:
	
	  - _ProcessStdout: called while a command is run to process any
	  data it outputs to stdout.
	  - _ProcessStderr: called while a command is run to process any
	  data it outputs to stderr.

	@ivar command: argv list for command to run
	@type command: list of strings
	"""
	def __init__(self, *args, **kwargs):
		"""Constructor

		There are two ways to initialize a Command class: by passing
		the argv list directly, or by supplying command as a string
		using the command keyword argument. In that case the command
		will be tokenized automatically using strtools.Tokenize().

		Since Command will execute the command directly it is not
		vulnerable to shell escape tricks. However it will also not
		do glob expansion; to do this you will have to run the
		command via a shell (/bin/sh -c "command") or expand the
		glob manually.
		"""
		if args and kwargs.has_key("command"):
			raise AttributeError, \
				"cannot supply both an argument list and the command keyword argument"

		if args:
			self.command=args
		elif kwargs.has_key("command"):
			self.command=strtools.Tokenize(kwargs["command"])
	

	def _TryRead(self, fd):
		"""Try to read data from a file descriptor.

		@param fd: file descriptor to read from
		@type  fd: integer
		@return: data read or None if no data is available)
		@rtype:  string
		"""
		try:
			return os.read(fd, 4096)
		except OSError, e:
			if e.errno==errno.EAGAIN:
				return None
			raise e


	def _ProcessStdout(self, data):
		"""Process data received via stdout.

		The function can be used to process data received via
		stdout. In this base class it does nothing.

		@param data: data to process
		@type  data: string
		"""
		pass
	

	def _ProcessStderr(self, data):
		"""Process data received via stderr.

		The function can be used to process data received via
		stderr. In this base class it does nothing.

		@param data: data to process
		@type  data: string
		"""
		pass


	def _RunChild(self, stdin, stdout, stderr):
		"""The child part of running a command.

		This function takes care of setting up the environment in
		which to run the command and executed the command itself.

		@param  stdin: pipe fds to use for stdin
		@type   stdin: (read-fd, write-fd) tuple
		@param stdout: pipe fds to use for stdout
		@type  stdout: (read-fd, write-fd) tuple
		@param stderr: pipe fds to use for stderr
		@type  stderr: (read-fd, write-fd) tuple
		"""
		os.close(stdin[1])
		os.dup2(stdin[0], 0)
		os.close(stdout[0])
		os.dup2(stdout[1], 1)
		os.close(stderr[0])
		os.dup2(stderr[1], 2)
		os.execvp(self.command[0], self.command)
		sys.exit(0)


	def _RunParent(self, pid, stdin, stdout, stderr):
		"""The parent part of running a command.

		Waits for the command being run by the child process
		to finish while processing any data received from it.

		@param  stdin: pipe fds to use for stdin
		@type   stdin: (read-fd, write-fd) tuple
		@param stdout: pipe fds to use for stdout
		@type  stdout: (read-fd, write-fd) tuple
		@param stderr: pipe fds to use for stderr
		@type  stderr: (read-fd, write-fd) tuple
		@return: exitcode returned by command
		@rtype:  integer
		"""
		os.close(stdin[0])
		_SetFdFlag(stdin[1], os.O_NONBLOCK)
		os.close(stdout[1])
		_SetFdFlag(stdout[0], os.O_NONBLOCK)
		os.close(stderr[1])
		_SetFdFlag(stderr[0], os.O_NONBLOCK)

		exit=0
		while exit<2:
			data=self._TryRead(stdout[0])
			if data!=None:
				self._ProcessStdout(data)

			data=self._TryRead(stderr[0])
			if data!=None:
				self._ProcessStderr(data)

			if exit>0:
				exit=2
			else:
				(pid, status)=os.waitpid(pid, os.WNOHANG)
				if pid and os.WIFEXITED(status):
					exit=1

		os.close(stdin[1])
		os.close(stdout[0])
		os.close(stderr[0])

		return os.WEXITSTATUS(status)


	def Run(self):
		"""Run the command

		@return: exitcode returned by command
		@rtype:  integer
		"""
		stdin=os.pipe()
		stdout=os.pipe()
		stderr=os.pipe()

		pid=os.fork()
		if pid==0:
			self._RunChild(stdin, stdout, stderr)
		else:
			return self._RunParent(pid, stdin, stdout, stderr)


class OutputCommand(Command):
	"""Command executor with output processing

	This class works exactly like Command, but data output to
	stdout and stderr is captured for later usage.

		  >>> import dhm.command
		  cmd=dhm.command.OutputCommand(command="echo Hello, world")
		  >>> cmd.Run()
		  0
		  >>> cmd.stdout
		  'Hello, world'

	@ivar stdout: Data received via stdout
	@type stdout: string
	@ivar stderr: Data received via stderr
	@type stderr: string
	"""

	def _ProcessStdout(self, data):
		self.stdout+=data
	

	def _ProcessStderr(self, data):
		self.stderr+=data


	def Run(self):
		self.stdout=""
		self.stderr=""
		return Command.Run(self)

